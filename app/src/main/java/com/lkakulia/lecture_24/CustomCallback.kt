package com.lkakulia.lecture_24

interface CustomCallback {
    fun onFailure(error: String?)
    fun onResponse(response: String)
}