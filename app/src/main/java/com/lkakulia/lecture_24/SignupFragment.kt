package com.lkakulia.lecture_24

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_signup.view.*
import org.json.JSONObject

class SignupFragment : Fragment() {

    private lateinit var itemView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_signup, container, false)
        init()
        return itemView
    }

    private fun init() {
        itemView.signupButton.setOnClickListener {
            val email = itemView.emailEditText.text.toString()
            val password = itemView.passwordEditText.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty()) {
                itemView.signupButton.isClickable = false
                val parameters = mutableMapOf<String, String>()
                parameters["email"] = email
                parameters["password"] = password

                HttpRequest.postRequest(HttpRequest.REGISTER, parameters, object: CustomCallback {
                    override fun onFailure(error: String?) {
                        itemView.signupButton.isClickable = true
                        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(response: String) {
                        val json = JSONObject(response)
                        var token = ""
                        var id = ""

                        if (json.has("token"))
                            token = json.getString("token")

                        if (json.has("id"))
                            id = json.getInt("id").toString()

                        val transaction = fragmentManager?.beginTransaction()
                        val dashboardFragment = DashboardFragment()
                        val bundle = Bundle().apply {
                            putString("id", id)
                            putString("email", email)
                            putString("token", token)
                        }
                        dashboardFragment.arguments = bundle
                        transaction?.replace(R.id.registerFragment, dashboardFragment, "DashboardFragment")
                        transaction?.commit()
                    }

                })
            }
            else {
                Toast.makeText(context, "Please fill in all the fields", Toast.LENGTH_SHORT).show()
            }
        }
    }

}
