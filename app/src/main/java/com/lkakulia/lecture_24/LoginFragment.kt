package com.lkakulia.lecture_24

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_login.view.*
import org.json.JSONObject

class LoginFragment : Fragment() {

    private lateinit var itemView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_login, container, false)
        init()
        return itemView
    }

    private fun init() {
        itemView.loginButton.setOnClickListener {
            val email = itemView.emailEditText.text.toString()
            val password = itemView.passwordEditText.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty()) {
                itemView.loginButton.isClickable = false
                val parameters = mutableMapOf<String, String>()
                parameters["email"] = email
                parameters["password"] = password

                HttpRequest.postRequest(HttpRequest.LOGIN, parameters, object: CustomCallback {
                    override fun onFailure(error: String?) {
                        itemView.loginButton.isClickable = true
                        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(response: String) {
                        val json = JSONObject(response)
                        var token = ""

                        if (json.has("token"))
                            token = json.getString("token")

                        val transaction = fragmentManager?.beginTransaction()
                        val dashboardFragment = DashboardFragment()
                        val bundle = Bundle().apply {
                            putString("email", email)
                            putString("token", token)
                        }
                        dashboardFragment.arguments = bundle
                        transaction?.replace(R.id.loginFragment, dashboardFragment, "DashboardFragment")
                        transaction?.commit()
                    }

                })
            }
            else {
                Toast.makeText(context, "Please fill in all the fields", Toast.LENGTH_SHORT).show()
            }
        }
    }

}
