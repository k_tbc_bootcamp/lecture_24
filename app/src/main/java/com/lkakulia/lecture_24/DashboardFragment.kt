package com.lkakulia.lecture_24

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.fragment_dashboard.view.*

class DashboardFragment : Fragment() {

    private lateinit var itemView: View
    private var bundle: Bundle? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_dashboard, container, false)
        bundle = arguments
        init()
        return itemView
    }

    private fun init() {
        itemView.emailTextView.text = bundle?.getString("email")
        itemView.tokenTextView.text = bundle?.getString("token")

        if (!bundle?.getString("id").isNullOrEmpty())
            itemView.idTextView.text = bundle?.getString("id")
        else
            itemView.idTextView.visibility = View.GONE
    }

}
